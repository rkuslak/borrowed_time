Borrowed Time
===

Note: This project and open issues currently reside exclusively on  [gitlab](http://gitlabs.com/rkuslak/borrowed_time).

Borrowed Time is a simple app that attempts the automated creation of backups
for a given directory, allowing for the use of regexs against the file or
directory names to exclude them from inclusion. Archives are created in GZipped
tar files; the only format with univeral (command line) support on all current
major operating systems including Windows.

On invocation it will attempt to load a `config.yaml`, `config.yml` or
`config.json` configuration file, first from the current directory and if not
found from the systems configuration store. These paths are:

  * For Linux: `${HOME}/.config/borrowed_time`
  * For Windows: `%APPDATA%\\ronkuslak\\borrowed_time`
  * For macOS: `${HOME}/Library/Application Support/com.ronkuslak.borrowed_time`

The current configuration file format expressed in YAML is as follows:

```yaml
# This is an (optional) field to point to a local path to store all archives.
# If not defined defaults to 'backups' under the current directory:
archives_root_path: "./backups"
excluded_paths:
  # A list of regexs to apply against a file or directory name
  # If a match is found, the given file or directory is excluded
  # for example, to exclude an exact match of node_modules, add:
  # node_modules
  # You can similarly use it to exclude any directory ending in a string,
  # such as the following examples:
  - ".*\\.pyc$"
  - ".*\\.egg-info$"
  # (note, since this is a JSON or YAML file, escaping the string to ultimately
  # be valid in the format file type chosen is required)
archive_backups:
  - archive_name: "The name to prepend to the archive"
    archive_path: "The path to a directory holding files to archive"
    # The (optional) maximum number of archives to retain.
    # If when starting more than this number of backups exists, older
    # versions based on the date in the file name will be removed:
    maximum_backups: 5
  # Similarly, you can use any environment variables in substitution.
  # For example, you can back up your Firefox profiles as so.
  - archive_name: Firefox
    archive_path: ${HOME}/.mozilla/firefox
    maximum_backups: 2
    # You can also define excluded paths on a per-archive basis, simply
    # providing it with the archive definition. Again these are currently 
    # treated as regexes against the full path:
    excluded_paths:
      - ^foo$
      - .*bar
  # Note that Windows paths will need to be double-escaped, as YAML/JSON
  # will require it to be escaped and to allow for literals in the string
  # replacements we also use them as an escape character in these paths
  - archive_name: Firefox on Windows
    archive_path: "${APPDATA}\\\\Mozilla\\\\Firefox",
    maximum_backups: 2
```