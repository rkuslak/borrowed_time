/*
   Borrowed Time
   Copyright (C) 2020 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use crate::display::{push_to_current_line, Status};
use crate::errors::ArchiverError;
use crate::file_filter::FileFilter;
use chrono::Utc;
use regex::Regex;

use std::fs::{DirEntry, File};
use std::io::Write;
use std::path::{Path, PathBuf};

// List of backup archive extensions
const ZIP_EXTENSION: &'_ str = ".zip";
const TAR_EXTENSION: &'_ str = ".tar.gz";
const EXTENSIONS: &'_ [&'_ str] = &[&ZIP_EXTENSION, &TAR_EXTENSION];

// String template to use to format a given date into a timestamp
const TIMESTAMP_TEMPLATE: &'_ str = "%Y-%m-%d";

// Regular expression used to parse creation time from a given archive
const TIMESTAMP_REGEX_TEMPLATE: &'_ str = "([0-9]{4,6})-([0-9]{1,2})-([0-9]{1,2})";

type FileFilterFunc<'a> = &'a dyn Fn(&PathBuf) -> bool;
pub type FileAddError = (PathBuf, ArchiverError);

pub fn current_timestamp() -> String {
    let now = Utc::now();

    now.format(TIMESTAMP_TEMPLATE).to_string()
}

fn get_aged_files<T: AsRef<str>>(
    path_regex_str: T,
    backup_files: &Vec<DirEntry>,
    max_backups: usize,
) -> Option<Vec<PathBuf>> {
    let path_re = Regex::new(path_regex_str.as_ref()).unwrap();

    let found_backups: Vec<PathBuf> = backup_files
        .iter()
        .filter(|&backup_file| path_re.is_match(&backup_file.file_name().to_string_lossy()))
        .map(|de| de.path())
        .collect();

    if found_backups.len() < max_backups {
        return None;
    }

    // This will only work until the year 429496; after that you're on your own.
    type DatedPathDir<'a> = (u32, &'a PathBuf);

    // As we know we will have to sort, create a sort key for each filename based on the date.
    let mut found_backups: Vec<DatedPathDir> = found_backups
        .iter()
        .filter(|pb| pb.file_name().is_some())
        .map(|pb| {
            let filename = pb.file_name().unwrap().to_string_lossy();

            let matches = match path_re.captures(&filename) {
                Some(matches) => matches,
                _ => return None,
            };

            match (
                matches[1].parse::<u32>(),
                matches[2].parse::<u32>(),
                matches[3].parse::<u32>(),
            ) {
                (Ok(year), Ok(month), Ok(day)) => {
                    let mut sort_key: u32 = year * 1000;
                    sort_key += month * 100;
                    sort_key += day;

                    let result: DatedPathDir = (sort_key, pb);
                    return Some(result);
                }
                _ => None,
            }
        })
        .filter_map(|dpb| dpb)
        .collect();

    found_backups.sort_by(|a, b| a.0.cmp(&b.0));

    if found_backups.len() < max_backups {
        return None;
    }

    // Items returned are the ones to be dropped; as such, we only need to keep
    // the first several items bringing the total to large than the required
    // size:
    let elements_to_leave = found_backups.len() - max_backups;
    found_backups.truncate(elements_to_leave);
    Some(found_backups.iter().map(|dpb| dpb.1.clone()).collect())
}

pub fn remove_aged(
    archives_root_path: &Path,
    archive_name: &str,
    max_backups: Option<u16>,
) -> Result<(), ArchiverError> {
    // Additional charater to hold added divider
    let mut base_filename = String::new();
    base_filename.push_str(archive_name);
    base_filename.push('-');
    base_filename.push_str(TIMESTAMP_REGEX_TEMPLATE);

    let dir_entries = archives_root_path.read_dir()?;

    let mut files_to_delete = Vec::new();
    let backup_files: Vec<DirEntry> = dir_entries
        .filter_map(|rd| rd.ok())
        .collect::<Vec<DirEntry>>();

    for ext in EXTENSIONS.iter() {
        if let Some(max_backups) = max_backups {
            let mut path_glob = base_filename.clone();
            path_glob.push_str(ext);
            if let Some(aged_files) = get_aged_files(&path_glob, &backup_files, max_backups.into())
            {
                files_to_delete.extend(aged_files);
            }
        }
    }

    for file_to_delete in files_to_delete {
        std::fs::remove_file(file_to_delete)?;
    }
    Ok(())
}

fn open_tar_archive(
    tar_file: &File,
) -> Result<tar::Builder<libflate::gzip::Encoder<&File>>, ArchiverError> {
    let gzip_file = libflate::gzip::Encoder::new(tar_file)?;
    let tar_archive = tar::Builder::new(gzip_file);

    Ok(tar_archive)
}

fn get_files_in_path<A: Write>(
    root_path: &Path,
    files_path: &Path,
    file_filter: Option<FileFilterFunc>,
    archive: &mut tar::Builder<A>,
) -> Result<Vec<FileAddError>, ArchiverError> {
    let mut failed_files = Vec::<FileAddError>::new();
    let files_to_archive = files_path
        .read_dir()?
        .filter_map(|rd| rd.ok())
        .map(|de| de.path())
        .filter(|rd| file_filter.is_none() || file_filter.unwrap()(&rd));

    for path in files_to_archive {
        if path.is_dir() {
            if let Ok(files) = get_files_in_path(&root_path, &path, file_filter, archive) {
                failed_files.extend(files);
            }
            continue;
        }

        // Avoid attempting to store pipes, or other non-file "files"
        if !path.is_file() {
            continue;
        }

        let relative_path = path.strip_prefix(&root_path)?;

        let relative_path_str = match relative_path.to_str() {
            Some(s) => s.to_string(),
            _ => relative_path.to_string_lossy().to_string(),
        };

        push_to_current_line(&relative_path_str, Status::Pending);

        match add_file_to_tar_archive(archive, &root_path, &path) {
            Ok(_) => {
                push_to_current_line(&relative_path_str, Status::Suceeded);
            }
            Err(err) => {
                failed_files.push((path.to_path_buf(), err));
                push_to_current_line(&relative_path_str, Status::Failed);
            }
        }
    }

    Ok(failed_files)
}

pub fn create_archive<S, F>(
    archives_root_path: &Path,
    path_to_archive: &Path,
    file_filter: &F,
    archive_name: S,
) -> Result<Vec<FileAddError>, ArchiverError>
where
    S: AsRef<str>,
    F: FileFilter,
{
    if !path_to_archive.exists() {
        let err_msg = format!("Archive path {:?} does not exist", path_to_archive);
        return Err(ArchiverError::new(err_msg));
    }

    if !path_to_archive.is_dir() {
        let err_msg = format!(
            "Archive path {:?} is not a directory; can not archive",
            path_to_archive
        );
        return Err(ArchiverError::new(err_msg));
    }

    // TODO: Shouldn't test that we can write to the end destination before
    // starting?
    let mut tar_archive_filename = String::new();
    tar_archive_filename.push_str(archive_name.as_ref());
    tar_archive_filename.push_str(TAR_EXTENSION);

    let root_path = tempfile::tempdir()?;
    let temp_tar_path = root_path.path().join(tar_archive_filename.clone());
    let temp_tar_file = File::create(&temp_tar_path)?;
    let mut tar_archive = open_tar_archive(&temp_tar_file)?;

    let failed_files = get_files_in_path(
        &path_to_archive,
        &path_to_archive,
        Some(&|f| file_filter.filter_pathbuf(f)),
        &mut tar_archive,
    )?;

    // We need to explicitly finish the containing GZip writer, otherwise it
    // will simply write a empty file:
    tar_archive.into_inner()?.finish();

    // Due to IO issues, and to avoid overwriting a existing archive
    // while attempting to create a new one, we write all the archive
    // data to a "temp" file. Attempt to rename it, and if that fails we
    // will do a full copy
    let mut tar_path = archives_root_path.clone().to_path_buf();
    tar_path.push(tar_archive_filename);

    move_archive(temp_tar_path, tar_path)?;
    Ok(failed_files)
}

fn move_archive(temp_file: PathBuf, desired_path: PathBuf) -> Result<(), ArchiverError> {
    if let Err(_) = std::fs::rename(&temp_file, &desired_path) {
        // There are several situations in which a copy can fail, most
        // commonly if on Linux and the archive path and temp dir are on
        // different mount points. In this case, copy the archive
        std::fs::copy(&temp_file, &desired_path)?;

        // TODO: We may some day want to do something with this if we fail to
        // remove the temp file, but for now even if we fail to delete it the
        // system temp file processes should remove it regardless
        if let Err(_) = std::fs::remove_file(&temp_file) {};
    }

    Ok(())
}

fn add_file_to_tar_archive<T: Write>(
    tar_archive: &mut tar::Builder<T>,
    root_path: &Path,
    file_to_archive: &PathBuf,
) -> Result<(), ArchiverError> {
    let relative_path = file_to_archive.strip_prefix(&root_path)?;
    let mut input_file = File::open(&file_to_archive)?;

    tar_archive.append_file(relative_path, &mut input_file)?;
    Ok(())
}
