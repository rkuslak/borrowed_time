use std::io::Write;

use crossterm::cursor::{RestorePosition, SavePosition};
use crossterm::style::{Attribute, Color, Print, SetAttribute, SetForegroundColor};
use crossterm::{execute, queue};
use std::io::stdout;

#[cfg(target_os = "windows")]
const NEW_LINE: &'_ str = &"\r\n";
#[cfg(not(target_os = "windows"))]
const NEW_LINE: &'_ str = &"\n";

const SUCCESS_CHAR: char = '✔';
const FAILURE_CHAR: char = '⤫';
const PENDING_CHAR: char = '*';

pub enum Status {
    Pending,
    Failed,
    Suceeded,
}

impl Status {
    pub fn char(&self) -> char {
        match self {
            Status::Pending => PENDING_CHAR,
            Status::Failed => SUCCESS_CHAR,
            Status::Suceeded => FAILURE_CHAR,
        }
    }

    pub fn color(&self) -> Color {
        match self {
            Status::Pending => Color::Yellow,
            Status::Failed => Color::Red,
            Status::Suceeded => Color::Green,
        }
    }
}

pub fn push_to_current_line<S>(msg: S, status: Status)
where
    S: AsRef<str>,
{
    let msg: &str = msg.as_ref();

    // TODO: Is this recoverable? Would this fail on a non-login PTY? Do we need
    // to handle it?
    match failable_push_to_current_line(msg, status) {
        Err(_) => panic!("stdout is unavailable!!!"),
        _ => (),
    }
}

pub fn report_failure<S>(archive_name: S, reason: S)
where S: AsRef<str> {
    let archive_name = archive_name.as_ref();
    let reason = reason.as_ref();

    let mut stdout = stdout();

    if let Err(_) = {
        queue!(
            stdout,
            SetForegroundColor(Color::Red),
            Print(' '),
            Print(FAILURE_CHAR),
            Print("  "),
            SetAttribute(Attribute::Reset),
            SetForegroundColor(Color::Grey),
            Print("\""),
            SetForegroundColor(Color::Red),
            SetAttribute(Attribute::Bold),
            Print(archive_name),
            SetAttribute(Attribute::Reset),
            SetForegroundColor(Color::Grey),
            Print("\": "),
            SetForegroundColor(Color::White),
            SetAttribute(Attribute::Bold),
            Print(reason),
            SetAttribute(Attribute::Reset),
            Print(NEW_LINE),
        )
    } {
        // Output failed - default to common
        println!("\tFailed to create {} - {}", archive_name, reason);
    }
}

fn failable_push_to_current_line(msg: &str, status: Status) -> Result<(), crossterm::ErrorKind> {
    let mut stdout = stdout();

    queue!(
        stdout,
        SavePosition,
        SetForegroundColor(Color::Grey),
        Print("["),
        SetForegroundColor(status.color()),
        Print(status.char()),
        SetForegroundColor(Color::Grey),
        Print("] "),
        SetForegroundColor(Color::White),
        SetAttribute(Attribute::Bold),
        Print(msg),
        SetAttribute(Attribute::Reset),
    )?;

    match status {
        Status::Pending => queue!(stdout, RestorePosition)?,
        _ => queue!(stdout, Print(NEW_LINE))?,
    };

    stdout.flush()?;
    Ok(())
}

pub fn set_current_archive<S>(archive_name: S)
where
    S: AsRef<str>,
{
    let archive_name = archive_name.as_ref();

    let result = execute!(
        stdout(),
        SetAttribute(Attribute::Reset),
        SetForegroundColor(Color::White),
        Print("Creating archive \""),
        SetAttribute(Attribute::Bold),
        SetForegroundColor(Color::Green),
        Print(archive_name),
        SetAttribute(Attribute::Reset),
        SetForegroundColor(Color::White),
        Print("\""),
        Print(NEW_LINE),
    );

    if let Err(_) = result {
        // Formatted printing failed; attempt just standard print:
        println!("Creating archive {}", archive_name);
    };
}
