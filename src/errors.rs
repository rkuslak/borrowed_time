/*
   Borrowed Time
   Copyright (C) 2020 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::fmt::{Display, Formatter};

pub struct ArchiverError {
    pub msg: String,
    // TODO: We should retain the source error or at least hold an source error
    // type, if possible.
}

impl ArchiverError {
    pub fn new<T: AsRef<str>>(err_msg: T) -> Self {
        ArchiverError {
            msg: String::from(err_msg.as_ref()),
        }
    }
}

impl Display for ArchiverError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.msg)
    }
}

impl std::fmt::Debug for ArchiverError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.msg)
    }
}

impl From<serde_yaml::Error> for ArchiverError {
    fn from(err: serde_yaml::Error) -> Self {
        let mut err_msg = format!("Failed to decode yaml: {}", err.to_string());
        if let Some(loc) = err.location() {
            err_msg = format!("{}: LINE: {}, COL: {}", err_msg, loc.line(), loc.column());
        }

        return Self::new(err_msg)
    }
}

impl From<serde_json::Error> for ArchiverError {
    fn from(err: serde_json::Error) -> Self {
        let line = err.line();
        let col = err.column();
        let cause_type = match err.classify() {
            serde_json::error::Category::Io => "IO failure to json file",
            serde_json::error::Category::Eof => "File ended before closing json object",
            serde_json::error::Category::Data => {
                "Wrong data type provided for field, or missing field"
            }
            serde_json::error::Category::Syntax => "Invalid syntax",
        };

        let err_msg = format!("Error: line {}, column {}, {}.", line, col, cause_type);
        ArchiverError { msg: err_msg }
    }
}

impl From<std::io::Error> for ArchiverError {
    fn from(err: std::io::Error) -> Self {
        let err_msg = format!("Error opening file: {}", err.to_string());
        ArchiverError { msg: err_msg }
    }
}

impl From<std::path::StripPrefixError> for ArchiverError {
    fn from(err: std::path::StripPrefixError) -> Self {
        let err_msg = format!("Failed to strip path prefix: {:?}", err);
        ArchiverError { msg: err_msg }
    }
}
