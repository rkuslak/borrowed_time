use regex::Regex;
use std::path::PathBuf;

pub struct RegexFileFilters {
    excluded_file_regex: Vec<Regex>,
}

pub trait FileFilter {
    fn filter<S: AsRef<str>>(&self, filename: S) -> bool;
    fn filter_pathbuf(&self, pathbuf: &PathBuf) -> bool;
}

impl<I, T> From<I> for RegexFileFilters
where
    I: IntoIterator<Item = T>,
    T: AsRef<str>,
{
    fn from(filters: I) -> Self {
        let excluded_file_regex: Vec<Regex> = filters
            .into_iter()
            .map(|f| Regex::new(f.as_ref()))
            .filter_map(|f| f.ok())
            .collect::<_>();

        RegexFileFilters {
            excluded_file_regex,
        }
    }
}

impl RegexFileFilters {
    pub fn new(filters: &[Regex]) -> Self {
        let excluded_file_regex: Vec<Regex> = filters.iter().map(|f| f.clone()).collect::<_>();

        RegexFileFilters {
            excluded_file_regex,
        }
    }

    pub fn push(&mut self, regex: Regex) {
        self.excluded_file_regex.push(regex);
    }

    pub fn append_strs<I, S>(&mut self, regexes: I)
    where
        I: IntoIterator<Item = S>,
        S: AsRef<str>,
    {
        let valid_regexs = regexes
            .into_iter()
            .map(|s| Regex::new(s.as_ref()))
            .filter_map(|re| re.ok());

        for re in valid_regexs {
            self.push(re);
        }
    }
}

impl Clone for RegexFileFilters {
    fn clone(&self) -> Self {
        RegexFileFilters {
            excluded_file_regex: self.excluded_file_regex.iter().cloned().collect(),
        }
    }
}

impl FileFilter for RegexFileFilters {
    fn filter<S: AsRef<str>>(&self, filename: S) -> bool {
        self.excluded_file_regex
            .iter()
            .find(|re| re.is_match(filename.as_ref()))
            .is_none()
    }

    fn filter_pathbuf(&self, pathbuf: &PathBuf) -> bool {
        if let Some(filename) = pathbuf.file_name() {
            let filename = filename.to_string_lossy();
            return self.filter(&filename);
        }

        false
    }
}
