/*
   Borrowed Time
   Copyright (C) 2020 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

mod archiving;
mod display;
mod errors;
mod file_filter;
mod models;
mod options;
mod templating;

use std::{collections::HashMap, path::PathBuf};

type FailedArchiveErrorTuple = (String, String);

fn process_archives() -> Result<(), errors::ArchiverError> {
    let config_file = options::load_config_file()?;

    // If we can, let's make the root path absolute. This will fail if we're
    // using a Windows network path UNC, and if so we will just true the user
    // knows where they want to store it and that we can access it
    let mut archives_root_path = PathBuf::from(&config_file.archives_root_path);
    if let Ok(canonical_path) = archives_root_path.canonicalize() {
        archives_root_path = canonical_path;
    }

    if !archives_root_path.exists() {
        let err_msg = format!(
            "Root path \"{:?}\" does not exist or is unaccessable to us",
            archives_root_path
        );
        return Err(errors::ArchiverError::new(err_msg));
    }
    println!("Archives will be stored in {:?}", &archives_root_path);

    let current_timestamp = archiving::current_timestamp();
    let mut failed_archives: Vec<FailedArchiveErrorTuple> = Vec::new();
    let mut failed_files_map: HashMap<String, Vec<archiving::FileAddError>> = HashMap::new();

    let exclusion_regexs: Vec<regex::Regex> = config_file
        .excluded_paths
        .iter()
        .map(|s| regex::Regex::new(&s))
        .filter_map(|re| re.ok())
        .collect();

    let file_filter = file_filter::RegexFileFilters::new(&exclusion_regexs);

    for archive_backup in &config_file.archive_backups {
        let path_to_archive = PathBuf::from(&archive_backup.archive_path);

        let mut archive_name = String::new();
        archive_name.push_str(&archive_backup.archive_name);
        archive_name.push('-');
        archive_name.push_str(&current_timestamp);

        display::set_current_archive(&archive_name);

        let archiving_success = match &archive_backup.excluded_paths {
            Some(exclusion_regexs_strings) => {
                let mut combined_file_filter = file_filter.clone();
                combined_file_filter.append_strs(exclusion_regexs_strings);
                archiving::create_archive(
                    &archives_root_path,
                    &path_to_archive,
                    &combined_file_filter,
                    &archive_name,
                )
            }
            _ => archiving::create_archive(
                &archives_root_path,
                &path_to_archive,
                &file_filter,
                &archive_name,
            ),
        };

        match archiving_success {
            Err(err) => {
                let failed_archive_name = archive_backup.archive_name.to_string();
                let error_reason = err.to_string();
                failed_archives.push((failed_archive_name, error_reason))
            }
            Ok(failed_files) => {
                if let Err(err) = archiving::remove_aged(
                    &archives_root_path,
                    &archive_backup.archive_name,
                    archive_backup.maximum_backups,
                ) {
                    println!(
                        "Failed to remove aged archives for \"{:?}\": {:?}; ignoring...",
                        &archive_backup.archive_name, &err
                    );
                }
                if failed_files.len() > 0 {
                    failed_files_map.insert(archive_backup.archive_name.clone(), failed_files);
                }
            }
        }
    }

    for failed_archive in failed_archives {
        display::report_failure(failed_archive.0, failed_archive.1);
    }

    for key in failed_files_map.keys() {
        println!("Failed to add files for archive {}:", &key);
        if let Some(failed_files) = failed_files_map.get(key) {
            for failed_file in failed_files {
                let reason = failed_file.1.to_string();
                let archive_name = match failed_file.0.to_str() {
                    Some(s) => s.into(),
                    _ => failed_file.0.to_string_lossy().to_string(),
                };
                display::report_failure(&archive_name, &reason);
            }
        }
    }

    Ok(())
}

fn main() {
    match process_archives() {
        Ok(_) => std::process::exit(0),
        Err(e) => {
            println!("{:?}", e);
            std::process::exit(255)
        }
    };
}
