/*
   Borrowed Time
   Copyright (C) 2020 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::BufReader;

use crate::errors::ArchiverError;
use crate::options::get_env_map;
use crate::templating::resolve_variables;

fn default_archives_path() -> String {
    "./backups".into()
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct ArchiveBackup {
    pub archive_name: String,
    pub archive_path: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub maximum_backups: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub excluded_paths: Option<Vec<String>>,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct ConfigFile {
    #[serde(default = "default_archives_path")]
    pub archives_root_path: String,
    #[serde(default = "Vec::new", skip_serializing_if = "Vec::is_empty")]
    pub excluded_paths: Vec<String>,
    #[serde(default = "Vec::new", skip_serializing_if = "Vec::is_empty")]
    pub archive_backups: Vec<ArchiveBackup>,
    // pub rsync_backups: Option<Vec<RsyncBackup>>,
}

impl ConfigFile {
    pub fn from_file_path<F>(file_path: F) -> Result<ConfigFile, ArchiverError>
    where
        F: Into<std::path::PathBuf>,
    {
        let file_path = file_path.into();
        let config_file_json_file = File::open(&file_path)?;
        let config_file_reader = BufReader::new(config_file_json_file);

        match file_path.extension() {
            Some(ext) if ext == "json" => {
                let mut config_file: ConfigFile = serde_json::from_reader(config_file_reader)?;
                config_file.resolve_archive_paths();
                Ok(config_file)
            }
            Some(ext) if ext == "yaml" || ext == "yml" => {
                let mut config_file: ConfigFile = serde_yaml::from_reader(config_file_reader)?;
                config_file.resolve_archive_paths();
                Ok(config_file)
            }
            _ => Err(ArchiverError::new(
                "unable to file config file in local path or home directory",
            )),
        }
    }

    fn resolve_archive_paths(&mut self) {
        let env_vars = get_env_map();

        self.archives_root_path = resolve_variables(&self.archives_root_path, &env_vars);

        for path in &mut self.archive_backups {
            path.archive_path = resolve_variables(&path.archive_path, &env_vars);
        }
    }
}
