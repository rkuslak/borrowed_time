use std::path::PathBuf;
use std::collections::HashMap;
use crate::models::ConfigFile;
use crate::errors::ArchiverError;

pub fn get_env_map() -> HashMap<String, String> {
    let mut env_vars: HashMap<String, String> = HashMap::new();
    for var_os in std::env::vars_os().into_iter() {
        env_vars.insert(
            var_os.0.to_string_lossy().into(),
            var_os.1.to_string_lossy().into(),
        );
    }

    env_vars
}

pub fn load_config_file() -> Result<ConfigFile, ArchiverError> {
    let mut possible_config_file_locations: Vec<PathBuf> = vec![
        "config.json".into(),
        "config.yml".into(),
        "config.yaml".into(),
    ];

    if let Some(config_dir_path) = directories::ProjectDirs::from("com", "ronkuslak", "borrowed_time") {
        let cloned_paths = possible_config_file_locations.clone();

        for file_name in cloned_paths {
            let mut path_in_config_dir = config_dir_path.config_dir().clone().to_path_buf();
            path_in_config_dir.push(file_name);
            possible_config_file_locations.push(path_in_config_dir);
        }
    }

    for config_path in possible_config_file_locations.iter() {
        if !std::path::Path::new(&config_path).exists() {
            continue;
        }

        match  ConfigFile::from_file_path(&config_path) {
             Ok(config_file) => return Ok(config_file),
             Err(e) => return Err(e),
        };
       
    }

    Err(ArchiverError::new("Failed to load config file"))
}