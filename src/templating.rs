/*
   Borrowed Time
   Copyright (C) 2020 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::collections::HashMap;

static TERMINATOR_CHARACTERS: &'static [char] = &[' ', '\\', '/', '}'];

pub fn resolve_variables(arg: &str, environment: &HashMap<String, String>) -> String {
    let mut result = String::new();
    let mut chars = arg.char_indices();
    let mut is_escaped = false;
    let mut is_bracket = false;
    let mut var_name = String::new();

    // TODO: Currently this works, but to match shell escaping behavior we
    // likely want to adapt it to look for ${VAR_NAME}.
    while let Some((_, next)) = chars.next() {
        if is_escaped {
            result.push(next);
            is_escaped = false;
            continue;
        }

        if next == '\\' {
            is_escaped = true;
            continue;
        }

        if next != '$' {
            result.push(next);
            continue;
        }

        // $ indicates we're starting a variable name match;

        // See if we are in a bracketed variable:
        var_name.clear();
        if let Some((_, next)) = chars.next() {
            if next == '{' {
                is_bracket = true;
            } else {
                var_name.push(next);
            }
        }

        // Loop until we have no more characters, or we find a potential
        // variable terminator
        let mut next_char = chars.next();
        let mut next: char = next;
        let mut terminated = false;

        loop {
            if next_char.is_none() {
                break;
            }

            next = next_char.unwrap().1;
            if TERMINATOR_CHARACTERS.iter().find(|&&x| x == next).is_some() {
                terminated = true;
                break;
            }

            var_name.push(next);
            next_char = chars.next();
        }

        // The only time we wouldn't want to check this is if we terminated, are
        // closing a bracket, and the terminator is not a closing bracket
        if !terminated || !is_bracket || next == '}' {
            if let Some(value) = &environment.get(&var_name) {
                result.push_str(value);

                // If we had a terminator, we still potentially need to deal
                // with the next character; ensure we properly handle being
                // escaped or consuming the closing bracket:
                if terminated {
                    if next == '\\' {
                        is_escaped = true;
                    } else if !is_bracket {
                        result.push(next);
                    }
                }
                continue;
            }
        }

        // If matches failed and next_char is some we broke out due to
        // matching a terminator; add it
        result.push('$');
        if is_bracket {
            result.push('{');
        }
        result.push_str(&var_name);
        if terminated {
            result.push(next);
        }
    }

    result
}
